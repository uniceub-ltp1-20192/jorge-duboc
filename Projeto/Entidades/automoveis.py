class Automoveis:

    def __init__(self, identificador = 0, marca="", potencia=0, cor="", velocidade=0):
        self._identificador = identificador
        self._marca = marca
        self._potencia = potencia
        self._cor = cor
        self._velocidade = velocidade

    @property
    def identificador(self):
        return self._identificador

    @identificador.setter
    def identificador(self, identificador):
        self._identificador = identificador

    @property
    def marca(self):
        return self._marca

    @marca.setter
    def marca(self, marca):
        self._marca = marca

    @property
    def potencia(self):
        return self._potencia

    @potencia.setter
    def potencia(self, potencia):
        self._potencia = potencia

    @property
    def cor(self):
        return self._cor

    @cor.setter
    def cor(self, cor):
        self._cor = cor

    @property
    def velocidade(self):
        return self._velocidade

    @velocidade.setter
    def velocidade(self, velocidade):
        self._velocidade = velocidade

    def correr(self):
        print("rommmmmm")

    def mudarMarcha(self):
        print("mudei de marcha")


    

