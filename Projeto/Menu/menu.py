from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.cupe import Cupe

class Menu:

    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")

    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("entrou em Consultar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)
                        else:
                            print("""Nao foi encontrado nenhum registro com esse identificador""")
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if (retorno != None):
                            print(retorno)
                        else:
                            print("""Nao foi encontrado nenhum registro com esse atributo""")

                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""

            elif opMenu == "2":
                print("entrou em Inserir")
                Menu.menuInserir(d)
            elif opMenu == "3":
                print("entrou em alterar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno, d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscarPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("entrou em deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opcao valida")

    @staticmethod
    def menuBuscaPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d :Dados):
        retorno = d.buscarPorAtributo(input("Informe a marca para busca:"))
        return retorno

    def menuInserir(d):
        c = Cupe()
        c.cor = input("Informe uma cor: ")
        c.marca = input("Informe a marca: ")
        c.numero_de_portas = input("Informe o numero de portas: ")
        c.potencia = input("Informe a potencia: ")
        c.velocidade = input("Informe a velocidade: ")
        d.inserir(c)

    def menuAlterar(retorno, d):
        retorno.cor = Validador.validarValorInformado(retorno.cor, "informe uma cor: ")
        retorno.marca = Validador.validarValorInformado(retorno.marca, "informe a marca: ")
        retorno.numero_de_portas = Validador.validarValorInformado(retorno.numero_de_portas, "informe o numero de portas: ")
        retorno.potencia = Validador.validarValorInformado(retorno.potencia, "informe a potencia: ")
        retorno.velocidade = Validador.validarValorInformado(retorno.velocidade, "informe uma velocidade: ")
        d.alterar(retorno)

    def menuDeletar(retorno, d):
        print(retorno)
        deletar = input("""Deseja deletar o registro: 
                                S - Sim
                                """)
        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("Registro deletado")
        else:
            print("Registro nao foi deletado")

